# $name: The filename "${name}.list" will be created in the apt sources directory.
#
# $flavor: one of 'debian' or 'ubuntu'.
#   - Required.
#   - Default: 'debian'
#
# $distributions: The Debian distriubution to include. Typically a Debian code name
#   like "stretch", "sid", etc.
#   - Required.
#   - Default: []
#
# $comment: [REQUIRED] A comment describing the source file. Understands carriage returns
# ("\n").
#   - Required.
#   - Default: none
#
# $repository:
#   - Required.
#   - Default: 'http://debian.stanford.edu/debian/'
#
# $archive: The Debian archive inside the repository. Example:
# 'debian', 'debian-stanford', etc.
#
#   - Required.
#   - Default: 'debian'
#
#
#
# EXAMPLE 1.
#    su_apt::source { 'debian-local':
#      comment       => "The Stanford restricted Debian archive and source tree.",
#      repository    => 'http://debian.stanford.edu',
#      archive       => 'debian-stanford',
#      distributions => ['stretch-acs-dev', 'stretch-acs-test'],
#    }
#
# This example creates the file
# /etc/apt/sources.list.d/debian-local.list with this content:
#
#   # Managed by Puppet: DO NOT EDIT!
#
#   # The Stanford restricted Debian archive and source tree.
#
#   deb     http://debian.stanford.edu/debian-stanford stretch-acs-dev main non-free contrib
#   deb-src http://debian.stanford.edu/debian-stanford stretch-acs-dev main non-free contrib

#   deb     http://debian.stanford.edu/debian-stanford stretch-acs-test main non-free contrib
#   deb-src http://debian.stanford.edu/debian-stanford stretch-acs-test main non-free contrib
#
# EXAMPLE 2.
#    su_apt::source { 'ubuntu':
#      flavor        => 'ubuntu',
#      comment       => "The Stanford Ubuntu archive and source tree.",
#      archive       => 'ubuntu',
#      distributions => ['saucy'],
#    }
#
# This example creates the file
# /etc/apt/sources.list.d/ubuntu.list with this content:
#
#   # Managed by Puppet: DO NOT EDIT!
#
#   # The Stanford Ubuntu archive and source tree.
#   deb     http://debian.stanford.edu/ubuntu saucy main multiverse restricted universe
#   deb-src http://debian.stanford.edu/ubuntu saucy main multiverse restricted universe

define su_apt::source(
  String $flavor               = 'debian',
  Array[String] $distributions = [],
  String $comment              = undef,
  String $repository           = 'http://debian.stanford.edu',
  String $archive              = 'debian',
  #
  String $sources_dir          = '/etc/apt/sources.list.d',
  Hash $include                = {
  		                   'src' => true,
                                   'deb' => true,
                                 },
  Optional[String] $components = undef,
) {

  file { "${sources_dir}/${name}.list":
    content => template('su_apt/source.list.erb'),
  }

}
